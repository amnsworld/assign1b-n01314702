﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignement1b_AmandeepSingh
{
    public class Services
    {
        public bool pickup;
        public List<string> addons;
        public List<string> meals;
        public string paymentMode;

        public Services(bool up,List<string> add,List<string> meal, string pm)
        {
            pickup = up;
            addons = add;
            meals = meal;
            paymentMode = pm;
        }

    }
}