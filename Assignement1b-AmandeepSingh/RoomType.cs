﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignement1b_AmandeepSingh
{
    public class RoomType
    {
        //This is a public class with orivate variables & property accessors

        //Variables will store values related to Room
        private string selectedRoomType;
        private int numberOfGuests;

        public string SelectedRoomType
        {
            get { return selectedRoomType; }
            set { selectedRoomType = value; }
        }

        public int NumberOfGuests
        {
            get { return numberOfGuests; }
            set { numberOfGuests = value; }
        }
    }
}