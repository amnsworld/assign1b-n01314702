﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignement1b_AmandeepSingh
{
    public class Reservation
    {
        //Task: we are going to write a class which encapsulates the other classes.So, this i sthe class which will perform rhis task.

        public Customer customer;
        public RoomType roomType;
        public Services services;
        public Dates dates;

        //constructor to get values for the other class objects
        public Reservation(Customer cust, RoomType rt, Services serv, Dates d)
        {
            customer = cust;
            roomType = rt;
            services = serv;
            dates = d;
        }

        public string PrintReceipt()
        {
            string message = "Hotel Reservation Details: <br />";
            message += "Name: " + customer.customerName + "<br />";
            message += "Address: " + customer.customerAddress + "<br />";
            message += "Email: " + customer.customerEmail + "  Phone : " + customer.customerPhone + "<br />";
            message += "Room Type: " + roomType.SelectedRoomType + " for " + roomType.NumberOfGuests + " Guest(s) <br/>";
            message += "Check-In Date: " + dates.checkinDate.ToShortDateString() + "  Check-Out Date: " + dates.checkoutDate.ToShortDateString() + "<br />";

            if (services.pickup == true)
            {
                message += "Pickup is Required <br/>";
            }
            else
            {
                message += "Pickup Not Required <br/>";
            }

            if (services.meals.Count() > 0)
            {
                message += "Meals included : " +string.Join(",",services.meals.ToArray()) + "<br />" ;
            }

            if (services.addons.Count() > 0)
            {
                message += "Extra Services: " + string.Join(",", services.addons.ToArray()) + "<br />";
            }

            message += "<br/>";
            message += "Your total is $" + CalculateTotal().ToString() + "<br/>";

            if (services.paymentMode == "Pay At Hotel")
            {
                message += "Please Pay At Hotel.<br/>";
            }
            else
            {
                message += "Please Pay via Credit/Debit.<br/>";
            }
            return message;
        }

        public double CalculateTotal()
        {
            double total = 0;
            if (roomType.SelectedRoomType == "Deluxe Room")
            {
                total = 5;
            }
            else if (roomType.SelectedRoomType == "Super Deluxe Room")
            {
                total = 8;
            }
            else if(roomType.SelectedRoomType == "Tripple Bedded Room")
            {
                total = 12;
            }
            else if (roomType.SelectedRoomType == "Family Suite")
            {
                total = 15;
            }


            //Total based on number of guests
            if (roomType.NumberOfGuests > 2)
            {
                total = total * (roomType.NumberOfGuests / 2);
            }
            
            return total;
        }
    }
}