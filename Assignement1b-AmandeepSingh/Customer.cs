﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignement1b_AmandeepSingh
{
    public class Customer
    {
        //This is a public class with constructor

        //Following fields will be associated with the custoer making a booking.
        public string customerName;
        public string customerAddress;
        public string customerEmail;
        public string customerPhone;
        public string customerIDProof;


        //this is a public constructor taking parameters.
        public Customer(string cn, string ca, string ce, string cp, string cid)
        {
            customerName = cn;
            customerAddress = ca;
            customerEmail = ce;
            customerPhone = cp;
            customerIDProof = cid;
        }

    }
}