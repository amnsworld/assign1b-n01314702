﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignement1b_AmandeepSingh
{
    public partial class HotelReservation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DataSubmitted(object sender, EventArgs e)
        {
            //Reffered your example for server controls
            if (!Page.IsValid)
            {
                //if there is any data invalid or there is any exception, we will stop further processing and throw the appropriate exception
                return;
            }

            /* Task: assign each value from your severcontrol into a variable. Use the correct variables as they correspond to the data type.*/

            /*Creating Customers object */
            string name = customerName.Text.ToString();
            string address = customerAddress.Text.ToString();
            string email = customerEmail.Text.ToString();
            string phone = customerContactNumber.Text.ToString();
            string idproof = customerIdProof.Text.ToString();

            Customer cust = new Customer(name, address, email, phone, idproof);


            //Creating Room object 
            string selectedRoomType = roomType.SelectedItem.Text.ToString();   //from the room type dropdown this will store the value for the selected item
            int numberOfGuests = Convert.ToInt16(roomNumberOfGuests.SelectedItem.Value); //since number of guests will be integer we are converting the selected value to integer

            RoomType rt = new RoomType();
            rt.SelectedRoomType = selectedRoomType.ToString();
            rt.NumberOfGuests = numberOfGuests;


            //Creating Service object
            List<string> addons = new List<string>();
            foreach (Control control in cbServiceAddons.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox cb = (CheckBox)control;
                    if (cb.Checked == true)
                    {
                        addons.Add(cb.Text);
                    }
                }
            }

            List<string> meals = new List<string>();
            foreach (Control control in cbServiceMeals.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox cb = (CheckBox)control;
                    if (cb.Checked == true)
                    {
                        meals.Add(cb.Text);
                    }
                }
            }

            bool pickupRequired;
            if (pickupyes.Checked == true)
            {
                pickupRequired = true;
            }
            else
            {
                pickupRequired = false;
            }

            string paymentMode;
            if (ServicePaymentModePayAtHotel.Checked == true)
            {
                paymentMode = "Pay At Hotel";
            }
            else
            {
                paymentMode = "Credit/Debit";
            }

            Services serv = new Services(pickupRequired, addons, meals, paymentMode);

            //Creating Date object
            string customerCheckInDate = dateCheckIn.Text.ToString();
            DateTime checkinDate = DateTime.Parse(customerCheckInDate);

            string customerCheckOutDate = dateCheckOut.Text.ToString();
            DateTime checkoutDate = DateTime.Parse(customerCheckOutDate);

            Dates dates = new Dates(checkinDate, checkoutDate);


            //Making the object of wrapper class and assigning the values of objects
            Reservation newreservation = new Reservation(cust, rt, serv, dates);

            PrintReceipt.InnerHtml = newreservation.PrintReceipt();

        }

    }
}