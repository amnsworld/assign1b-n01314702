﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelReservation.aspx.cs" Inherits="Assignement1b_AmandeepSingh.HotelReservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <form id="form1" runat="server">
        <div>
            <%--how to align label text to centre in asp reffered from 
                https://stackoverflow.com/questions/13797492/how-to-align-text-to-center-in-asp-label  --%>

            <div style="margin-left: auto; margin-right: auto; text-align: center;">
             <asp:Label ID="lblGotelBookingZone" runat="server" Text="Hotel Booking Zone" Font-Bold="true" Font-Size="X-Large" CssClass="StrongText"></asp:Label>
            </div>
            <br />

            <asp:Label runat="server">Name: </asp:Label><asp:TextBox runat="server" id="customerName" placeholder="e.g. John doe"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter name." ForeColor="Red" ControlToValidate="customerName" ID="validateCustomerName"></asp:RequiredFieldValidator>
            <br />
            <br />

            <asp:Label runat="server">Address: </asp:Label><asp:TextBox runat="server" ID="customerAddress" placeholder="e.g. 123, Canada"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter address" ForeColor="Red" ControlToValidate="customerAddress" ID="validateCustomerAddress"></asp:RequiredFieldValidator>
            <br />
            <br />

            <asp:Label runat="server">Email: </asp:Label><asp:TextBox runat="server" ID="customerEmail" placeholder="e.g. johndoe@example.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ForeColor="Red" ControlToValidate="customerEmail" ID="validateCustomerEmailAddress"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="validateCustomerEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="customerEmail" ForeColor="Red" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Label runat="server">Contact number: </asp:Label><asp:TextBox runat="server" ID="customerContactNumber" placeholder="10-12 digit contact number"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ForeColor="Red" ControlToValidate="customerContactNumber" ID="validatorCustomerContactNumber" Display="Dynamic"></asp:RequiredFieldValidator>
           
            <%-- to validate pone number reffered to an article "Validate Mobile Number With 10 Digits In ASP.NET"
                By Prashant Dubey
                https://www.c-sharpcorner.com/blogs/validate-mobile-number-with-10-digits-in-asp-net --%>

            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCustomerContactNumber" runat="server" ControlToValidate="customerContactNumber" ForeColor="Red" ErrorMessage="Enter a valid Contact Number" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>          
            <br />
            <br />

            <asp:Label runat="server">ID Proof: </asp:Label><asp:TextBox runat="server" ID="customerIdProof" placeholder="XXXXXXXXXXX"></asp:TextBox>
            <asp:RequiredFieldValidator ID="validateCustomerIDProof" runat="server" ErrorMessage="Please enter ID Proof number" ForeColor="Red" ControlToValidate="customerIdProof"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Label runat="server">Room Type</asp:Label>
            <asp:DropDownList runat="server" ID="roomType">
                <asp:ListItem Text="Deluxe Room" Value="D"></asp:ListItem>
                <asp:ListItem Text="Super Deluxe Room" Value="SD"></asp:ListItem>
                <asp:ListItem Text="Tripple Bedded Room" Value="TBR"></asp:ListItem>
                <asp:ListItem Text="Family Suite" Value="FS"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Label runat="server">Number Of Guests</asp:Label>
            <asp:DropDownList runat="server" ID="roomNumberOfGuests">
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                <asp:ListItem Text="9" Value="9"></asp:ListItem>
            </asp:DropDownList>
            <br />          
            <br />
            <%--
                Regular Expression for date format is reffered from https://www.regextester.com/96683
                and CompareValidator for date is reffered from stackoverflow By Ebad Masood
                https://stackoverflow.com/questions/9372901/asp-net-compare-validator-to-validate-date
                --%>
            <asp:Label runat="server">Check-In Date: </asp:Label><asp:TextBox runat="server" ID="dateCheckIn" placeholder="yyyy-mm-dd"></asp:TextBox>
            <asp:RequiredFieldValidator ID="validateDateCheckIn" runat="server" ForeColor="Red" ErrorMessage="Check-In Date Required"  ControlToValidate="dateCheckIn"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDateCheckIn" runat="server" ValidationExpression="([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))" ControlToValidate="dateCheckIn" ForeColor="Red" ErrorMessage="Invalid Date Format" Display="Dynamic"></asp:RegularExpressionValidator>
            
            <asp:Label runat="server">   Check-Out Date: </asp:Label><asp:TextBox runat="server" ID="dateCheckOut" placeholder="yyyy-mm-dd"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDateCheckOut" runat="server" ForeColor="Red" ErrorMessage="Check-Out Date Required" ControlToValidate="dateCheckOut" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDateCheckOut" runat="server" ValidationExpression="([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))" ControlToValidate="dateCheckOut" ForeColor="Red" ErrorMessage="Invalid Date Format"></asp:RegularExpressionValidator>
            
            <asp:CompareValidator id="compareValidatorDateCheckInOut" runat="server" 
             ControlToCompare="dateCheckIn" cultureinvariantvalues="true" 
             display="Dynamic" enableclientscript="true"  
             ControlToValidate="dateCheckOut" 
             ErrorMessage="Check-IN date must be earlier than Check-Out date"
             type="Date" setfocusonerror="true" Operator="GreaterThanEqual" 
             text="Check-IN date must be earlier than Check-Out date"></asp:CompareValidator>
            <br />
            <br />
            <asp:Label runat="server">Pickup Service Needed?</asp:Label>
            <asp:RadioButton runat="server" Text="Yes" GroupName="pickupService" ID="pickupyes" />
            <asp:RadioButton runat="server" Text="No" GroupName="pickupService" Checked="true" ID ="pickupno" />
            <br />  
            <br />
            <asp:Label runat="server">Meals: </asp:Label>
            <br />
            <div id="cbServiceMeals" runat="server">
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:CheckBox ID="cbServiceBreakfast" runat="server" Text="Breakfast" />
            <asp:CheckBox ID="cbServiceLunch" runat="server" Text="Lunch" />
            <asp:CheckBox ID="cbServiceDinner" runat="server" Text="Dinner" />
            </div>
            <br />
            <asp:Label runat="server">Add Ons: </asp:Label>
            <br />
            <div id="cbServiceAddons" runat="server">
             &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:CheckBox ID="cbServiceExtraMattress" runat="server" Text="Extra Mattress" />
            <asp:CheckBox ID="cbServiceCharger" runat="server" Text="Charger" />
            <asp:CheckBox ID="cbServiceDrinks" runat="server" Text="Drinks" />
            </div>
            <br />

            <asp:Label runat="server">Payment Mode</asp:Label>
            &nbsp&nbsp<asp:RadioButton runat="server" Text="Pay at Hotel" GroupName="paymentMode" ID="ServicePaymentModePayAtHotel" />
            <asp:RadioButton runat="server" Text="Credit/Debit" GroupName="paymentMode" Checked="true" ID="ServicePaymentModeCreditDebit" />

            <br />
            <br />
            <asp:Button runat="server" ID="myButton" OnClick="DataSubmitted" Text="Submit Details"/>
            <br />
            <br />
            <%-- took reference for Validation summary from 
                Kudvenkat channel on youtube 
                https://www.youtube.com/watch?v=HI-koNrINTc   --%>
            <asp:ValidationSummary ID="validationSummary" runat="server" HeaderText="Validation summary" />

            <div runat="server" id="PrintReceipt">

            </div>
        </div>
    </form>

</body>
</html>
